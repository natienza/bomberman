#ifndef BM_CLIENT_H
#define BM_CLIENT_H

#include <iostream>
#include <string.h>
#include <SFML/Network.hpp>

#include "BM_SLee.h"

using namespace std;
using namespace sf;


class BM_Client : public BM_SLee {
 public:
  BM_Client();
  ~BM_Client();
  void connect();
  void run();
  void joueurWatcher();
  void updateServeur();
  void receive();
 private :
  mutex mPaquet;
  thread threadReception;
  TcpSocket socket;
  string adressServer;// "176.184.211.155";
  int port;
  Packet paquet;
  int codeTouche = 0;
  thread updateThread;
};

#endif // BM_CLIENT_H
