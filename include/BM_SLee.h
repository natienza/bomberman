#ifndef BM_SLEE_H
#define BM_SLEE_H

#include <iostream>
#include <string.h>
#include <thread>
#include <mutex>
#include <SFML/Graphics.hpp>

using namespace std;
using namespace sf;
#include "BM_Case.h"
#include "BM_JStark.h"
#include "BM_Joueur.h"
#include "BM_Bombe.h"
#include "BM_Flamme.h"
#include "BM_Explosion.h"

#define TEXTURE_SIZE 36
#define NB_TEXTURE 14
#define PERIOD_JOUEUR_WATCHER 15 //ms
#define PERIOD_BOMB_WATCHER 1000 //ms
#define JOUEUR_SPEED 1

#define DECOR_NB_CASSABLE 70

//#define VERBOSE // + de cout

class BM_SLee
{
    public:
    /*Constructeur/Destructeur*/
        BM_SLee();
        BM_SLee(int newNLignes, int newNColonnes);
        BM_SLee(BM_SLee&&)=default;
        virtual ~BM_SLee();
    /*Setter*/
        void setDimX(int newNColonnes);
        void setDimY(int newNLignes);
    /*Getter*/
        BM_Case* getCase(int ligne, int colonne);
        void setCase(int ligne, int colonne, BM_Case* newCase);
        /*Gestion, des joueurs*/
        template <typename T_BM_Joueur>
        void addJoueur(T_BM_Joueur newJoueur);
        void delJoueur(int position);
    /*Gestion des bombes*/
          void addBombe(BM_Bombe* newBombe);
          void delBombe(int position);
    /*Gestion des flammes*/
          void addFlamme(BM_Flamme* newFlamme);
          void delFlamme(BM_Flamme* newFlamme, int position);
    /*Gestion des explosions*/
          void addExplosion(BM_Explosion* newExplosion);
          void delExplosion(BM_Explosion* newExplosion, int position);
    /*Thread de gestion des bombes*/
        void run(){
          bombThread = thread(&BM_SLee::bombWatcher, this);
          displayThread = thread(&BM_SLee::show, this);
          flameThread = thread(&BM_SLee::flameWatcher, this);
          joueurThread = thread(&BM_SLee::joueurWatcher, this);
          cout << "running SLee..." << endl;
          bombThread.join();
          displayThread.join();
          flameThread.join();
          joueurThread.join();
          }
    /*En attente de rangement*/
        int getNColonnes();
        string joueurToString();
        void afficherJoueur();
        string grilleToString();
        void afficherGrille();
        void bombWatcher();
        void show();
        void flameWatcher();
        void joueurWatcher();

    protected:
    /* Attributs */
        bool display=true;
        bool gameOn = true;
        thread bombThread;
        thread displayThread;
        thread flameThread;
        thread joueurThread;
        mutex mGrille;
        mutex flameMutex;
        int nLignes = 4;
        int nColonnes = 4;
        int nJoueurs = 0;
        int nBombe = 0;
        int nFlamme = 0;
        int nExplosion = 0;
        int periodFlammeWatcher = 250; //ms
        BM_Case** grille;
        BM_Joueur** tabJoueur;
        BM_Bombe** tabBombe;
        BM_Flamme** tabFlamme;
        BM_Explosion** tabExplosion;
        Texture* tabTexture[NB_TEXTURE];
        RenderWindow fenetre;
    /* Méthodes Privés */
        void genZoneJeu();

};

template <class T_BM_Joueur>
void BM_SLee::addJoueur(T_BM_Joueur newJoueur){
    BM_Joueur** buffer = new BM_Joueur*[nJoueurs+1];
    for (int i=0; i<nJoueurs; i++){
		buffer[i] = tabJoueur[i];
	}
    buffer[nJoueurs++] = new T_BM_Joueur(newJoueur);
    delete [] tabJoueur;
    tabJoueur = buffer;
    return;
}



#endif // BM_SLEE_H
