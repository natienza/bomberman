#ifndef BM_JSTARK_H
#define BM_JSTARK_H

#include <BM_Joueur.h>


class BM_JStark : public BM_Joueur
{
    public:
        BM_JStark();
        BM_JStark(const BM_Joueur& newJoueur);
        BM_JStark(const BM_JStark& newJoueur);
        virtual ~BM_JStark();
        string toString();
        void afficher();

    protected:

    private:
};

#endif // BM_JSTARK_H
