#ifndef EXPLOSION_H
#define EXPLOSION_H

#include <iostream>
#include <string.h>

using namespace std;

#include "BM_Flamme.h"
#include "BM_Joueur.h"

class BM_Explosion{

 public:
  BM_Explosion();
  BM_Explosion(const BM_Explosion& newExplosion);
  virtual ~BM_Explosion();

  void setDuree(float newDuree);
  float getDuree();
  int* getIndices();
  int getNFlammes();
  void addFlamme(BM_Flamme* flamme);
  bool containJoueur(BM_Joueur* joueur);
 private:
  BM_Flamme** tabFlamme;
  float duree = 1;
  int nFlamme=0;
};

#endif // EXPLOSION_H
