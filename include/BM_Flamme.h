#ifndef BM_FLAMME_H
#define BM_FLAMME_H

#include <iostream>
#include <string.h>
#include "BM_Case.h"

using namespace std;

class BM_Flamme : public BM_Case {

 public:
  /*Constructeur/Destructeur*/
  BM_Flamme();
  BM_Flamme(const BM_Flamme& newFlamme);
  virtual ~BM_Flamme();
  /*Comportement*/
  bool isWalkable();
  bool isBreakable();
  bool isFlammable();
  string toString();
  void afficher();
  int getIdCase();
  float getDuree();
  void setDuree(float newDuree);
  void setIdCase(int newId);

 private:
  float duree;
  char motif;
  string symbole = "***";
  int idCase;
};

#endif // BM_FLAMME_H
