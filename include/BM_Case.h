#ifndef BM_CASE_H
#define BM_CASE_H

#include <iostream>
#include <string.h>
#include <SFML/Graphics.hpp>
using namespace sf;
using namespace std;
enum TextureId {sol=0, briqueIncassable, briqueCassable, 
  bombe, feuBombe, feuH, feuV, feuEx, feuBrique,
  bonusBombe, bonusCoupPied, bonusFlamme, bonusRoller, persoJStark};

class BM_Case
{
    public:
    /*Constructeur/Destructeur*/
        BM_Case();
        BM_Case(string newNom);
        virtual ~BM_Case();
    /*Comportement*/
        virtual bool isBreakable()=0;
        virtual bool isWalkable()=0;
        virtual bool isFlammable()=0;
        void setPosition(int newLigne, int newColonne);
        void setIndexCase(int newIndex);
    /*Affichage*/
        void setTexture(string newTexture);
        void setTextureId(TextureId newId);
        string getTexture();
        TextureId getTextureId();
    /*Debuggage*/
        virtual string toString()=0;
        virtual void afficher()=0;

    protected:
        string nom;
        string texture;
        TextureId idTexture;
};

#endif // BM_CASE_H
