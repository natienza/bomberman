#ifndef BM_BRIQUECASSABLE_H
#define BM_BRIQUECASSABLE_H

#include <iostream>
#include <string.h>
#include "BM_Case.h"
using namespace std;

class BM_BriqueCassable : public BM_Case {

 public :
 /*Constructeur/Destructeur*/
  BM_BriqueCassable();
  BM_BriqueCassable(const BM_BriqueCassable& newBrique);
  ~BM_BriqueCassable();
 /*Comportement*/
  bool isWalkable();
  bool isBreakable();
  bool isFlammable();
 /*Debuggage*/
  string toString();
  void afficher();

 private:
  string symbole = " x ";
};

#endif // BM_BRIQUE_CASSABLE_H
