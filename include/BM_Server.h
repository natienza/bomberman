#ifndef BM_SERVER_H
#define BM_SERVER_H

#include <iostream>
#include <string.h>
#include <SFML/Network.hpp>

#include "BM_SLee.h"

using namespace std;
using namespace sf;


class BM_Server : public BM_SLee {

 public :
  BM_Server();
  ~BM_Server();
  void launch();
  void run();
  void updateJoueur();
  void receive(int i);
  void send(int i);
 private :
  bool display=true;
  mutex* mPaquet;
  thread* threadReception;
  thread* threadEmission;
  int nClients;
  TcpListener listener;
  TcpSocket* client;
  SocketSelector selector;
  int port;
  Packet* paquet;
  int* idClient;
  int* idTexture;
  int* codeTouche;

};

#endif // BM_SERVER_H
