#ifndef BM_BOMBE_H
#define BM_BOMBE_H

#include <iostream>
#include <string.h>
#include "BM_Case.h"
#include "BM_Joueur.h"
#include "BM_JStark.h"
using namespace std;

class BM_Bombe : public BM_Case {

 public :
 /*Constructeur/Destructeur*/
  BM_Bombe();
  BM_Bombe(const BM_Bombe& newBombe);
  ~BM_Bombe();
 /*Comportement*/
  bool isBreakable();
  bool isWalkable();
  bool isFlammable();
  void setDuree(int newDuree);
  void setPortee(int newPortee);
  void setMotif(char newMotif);
  void setId(int idPoseur);
  void setIdCase(int newId);
  void setPoseur(BM_Joueur* newPoseur);
  void alertPoseur();
 /*Getter*/
  int getDuree();
  int getPortee();
  char getMotif();
  int getIdCase();
  /*Debuggage*/ 
  string toString();
  void afficher();

 private:
  int duree;
  int portee;
  char motif;
  int idPoseur;
  BM_Joueur* poseur;
  string symbole = " * ";
  int idCase;
};

#endif // BM_BOMBE_H
