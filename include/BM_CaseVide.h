#ifndef BM_CASEVIDE_H
#define BM_CASEVIDE_H

#include <BM_Case.h>


class BM_CaseVide : public BM_Case
{
    public:
        BM_CaseVide();
        BM_CaseVide(string newNom);
        virtual ~BM_CaseVide();
    /*Comportement*/
        bool isWalkable();
        bool isBreakable();
        bool isFlammable();
    /*Debuggage*/
        string toString();
        void afficher();

    protected:

    private:
        string symbole;
};

#endif // BM_CASEVIDE_H
