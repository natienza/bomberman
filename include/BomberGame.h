#ifndef BOMBERGAME_H
#define BOMBERGAME_H

#include <iostream>
#include <string.h>
#include <SFML/Graphics.hpp>
using namespace std;

#include "BM_SLee.h"

class BomberGame
{
 public:
  BomberGame();
 BomberGame(BomberGame&&)=default;
  virtual ~BomberGame();
  void show();

 private:
  sf::RenderWindow fenetre;
  BM_SLee* partie;
  int nLignes = 5;
  int nColonnes = 7;
};

#endif // BOMBERGAME_H
