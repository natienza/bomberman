#ifndef BM_BRIQUEINCASSABLE_H
#define BM_BRIQUEINCASSABLE_H

#include <BM_Case.h>


class BM_BriqueIncassable : public BM_Case
{
    public:
        BM_BriqueIncassable();
        BM_BriqueIncassable(string newNom);
        virtual ~BM_BriqueIncassable();
        bool isBreakable();
        bool isWalkable();
        bool isFlammable();
        void afficher();
        string toString();

    protected:

    private:
        string symbole;
};

#endif // BM_BRIQUEINCASSABLE_H
