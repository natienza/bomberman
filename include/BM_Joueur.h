#ifndef BM_JOUEUR_H
#define BM_JOUEUR_H


#include <iostream>
#include <string.h>
using namespace std;

#include "BM_Case.h"

#define NB_MAX_BOMB 2

class BM_SLee;

class BM_Joueur
{
    public:
        BM_Joueur();
        BM_Joueur(const BM_Joueur& newJoueur);
        void setPosition(float newLigne, float newColonne);
        float* getPosition();
        void updatePosition(float deltaLigne, float deltaColonne);
        int getIdCase();
        void setCase(BM_Case* newCase);
        void setTexture(string newTexture);
        string getTexture();
        BM_Case* getCase();
        void setGrille(BM_SLee* newGrille);
        void getNBomb();
        void updateNBombe(int up);
        void setBomb();
        void setTextureId(TextureId newId);
        TextureId getTextureId();
        virtual string toString()=0;
        virtual void afficher()=0;
        virtual ~BM_Joueur();
    protected:
        BM_Case* actualCase;
        BM_SLee* grille;
        float position[2] = {0,0};
        TextureId idTexture;
        string texture = "texture/perso3.png";//BonusFlamme.bmp"; // A changer !
        string nom;
        int nBomb = 0;
        int idCase;
        
};

#endif // BM_JOUEUR_H
