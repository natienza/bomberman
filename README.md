![](texture/logo.jpg)

Bomberman Game
==

Ce projet propose une réécriture libre en c++ du jeux *Bomberman* initalement sorti en 1983. Ce travail a été réalisé dans le cadre d'un projet informatique académique du M2 FESup de l'ENS Paris-Saclay. L'objectif de ce projet est le développement complet d'un jeu analogue au Bomberman initial se jouant en réseaux. 

Etat Actuel du développement
-

Pour l'instant, le jeux présente les fonctionnalités suivantes :

* Le jeu peut posséder plusieurs joueurs de même apparence
* Chaque joueur a la possibilité de poser jusqu'à 3 bombes simultanément sur le plateau.
* chaque bombe explose au bout de 3s et détruit lors de son explosion bloc cassable et personnage
* Le jeu peut se joueur à 2 sur un même clavier
* Le jeu peut se jouer en réseaux sans limite du nombre de joueurs (la destruction des joueurs entre eux est mal gérée)

Installation du Projet
-

Le projet utilise la librairie graphique *SFML* (<https://www.sfml-dev.org/>). Cette librairie propose une alternative orientée objet simple d'utilisation à SDL. Il faudra donc s'assurer avant de télécharger le projet d'avoir cette librairie installée.


    sudo apt install libsfml-dev

Pour télécharger le projet dans un répertoire de travail, cloner le dépot git :

    git clone https://gitlab-ingelec.hopto.org/ens/bomberman.git


Le projet est fournit avec un *makefile* pour la compilation. Pour compiler le projet :

    cd bomberman/
    make -B
    
    
Trois fichiers binaires seront alors créés dans le répertoire de travail :

* `./bin/Debug/Bomberman` : Programme lançant le jeu local se jouant à 2 joueurs sur 1 clavier
* `./bin/Debug/Server` : Programme lançant un serveur de jeu (suivre les instructions du terminal pour la configuration du serveur)
* `./bin/Debug/Client` : Programme lançant un client (suivre les instructions du terminal pour la configuration de la connexion à un serveur)

Structure du Jeu
-

Le diagramme de classe complet du projet est accessible ![ici](UML/BombermanUML.pdf).
Le projet se divise en 11 classes définies dans les headers :

* `include/BM_SLee.h` : définie la classe du *Game master*. Cette classe est celle qui permet à tous les éléments du jeux d'intéragir ensemble.Cette classe est composée de plusieurs tableaux de pointeurs permettant le stockage de tous les éléments du jeu. A travers sa méthode `run()`, différents thread sont appelés afin de remplir toutes les tâches essentielles au jeux :

    * Un thread d'affichage (`displayThread` qui gère l'affichage de la grille de jeux 
    * Un thread d'interaction avec la fenetre (`joueurThread` qui permet de récupérer les évènements générés par la fenêtre. Cette méthode permet non seulement de récupérer les actions du joueur mais aussi de laisser la fenêtre communiquer avec l'OS.
    * Des threads de gestion de jeux (`flameThread`, `bombThread`) qui permettent des faire respecter les règles du jeu et de gérer l'existence des bombes sur le terrain.
    
 

* `include/BM_Case` : définie la classe abstraite étant l'entité élémentaire composant la grille. `BM_Case_Vide`, `BM_BriqueCassable`,`BM_BriqueIncassable`, `BM_Bombe` et `BM_Flamme` sont les classes pouvant être instanciées sur la grille.

* `include/BM_Joueur` : définie la classe abstraite du joueur. Pour l'instant seul un type de joueur peut être instancié dans le jeux : `BM_JStark`.

* `include/BM_Server` : définie la classe permettant de mettre en place un serveur de jeux. Cette classe hérite du *game master*. De cette façon, il suffit de réécrire la méthode `run()` pour permettre au jeux de tourner comme en solo tout en gérant la communication avec les clients. La communication avec les clients est réalisé à travers des Sockets TCP. A chaque client est attribué :
    * Une socket `client[i]` (instance de la classe TcpSocket de la lib sfml) qui contient l'adresse IP du client `i` ainsi que le port par lequel il s'est connecté.
    * Un paquet `paquet[i]` (instance de la classe `Packet` de la lib sfml) qui contient les données reçues ou envoyées au client. Attention à bien vider la paquet avec la méthode `clear()` avant d'écire dans le paquet. Le paquet est construit de la manière suivante :
        * En cas de réception du client :
            * Un seul entier `codeTouche` dont chaque bit est le reflet de l'activation d'une touche
            * Possibilité de rajouter d'autres informations comme la texture utilisée par le joueur, le type de joueur, un pseudo, ...
        * En cas d'envoi vers le client :
            * un entier pour chaque case du jeu étant l'image de la texture de la case
            * un entier pour chaque joueur étant l'image de la position du joueur sur la grille
    * Un mutex de paquet `mPaquet` (instance de la librairie standard `thread`) qui protège l'accès à l'écriture et à la lectuer du paquet.
    * Un thread de récepetion `threadReception[i]` qui gére la réception des données depuis le client `i`. 
    * Un thread d'émission `threadEmission[i]` qui gère l'envoi des données vers le client `i`.
    
 Chaque caractéristique est stockée dans un tableau de la taille du nombre de joueurs et chaque est identifié par sa position dans le tableau de socket. Cette classe possède aussi un mutex dit de *grille* (`mGrille`) qui permet de s'assurer que les données du jeux ne sont pas utilisées par deux threads en même temps. 
 
 * `include/BM_Client.h` : définie la classe implémentant un client du jeu. Cette classe hérite aussi de la classe `BM_SLee.h` de telle sorte de pouvoir se reposer sur les mêmes fonctions d'affichage que le *game master*. Cette classe propose donc une réécriture de la méthode `run()` dans laquelle les threads d'émission et de réception modifient à la volée les attributs du jeu hérité.
 
Capture d'écrans
-

![](img/Capture_3.png)
![](img/Capture_9.png)
![](img/Capture_4.png)
![](img/Capture_5.png) 
![](img/Capture_6.png)
![](img/Capture_7.png)

---


Problèmes connus
-

Voici une liste des problèmes connus à résoudre :

* La destruction d'un joueur par un autre est mal gérée
* Lorsque joué en réseaux, la latence de l'affichage chez le client peut (pas toujours) être importante (qq 100 ms)
* Crash de X11 :

Dans certains cas (difficilement reproductible), le client est crashé par la librairie graphique X11 et ce message d'erreur apparait :
 
    [xcb] Unknown sequence number while processing queue
    [xcb] Most likely this is a multi-threaded client and XInitThreads has not been called
    [xcb] Aborting, sorry about that.
    Client: ../../src/xcb_io.c:259: poll_for_event: Assertion `!xcb_xlib_threads_sequence_lost' failed.

Ce problème a été résolu dans la version jeu local en faisant appel à la fonction `XInitThreads()` mais il semble qu'elle ne soit pas compatible avec les modules network de *SFML*...
Afin d'éviter ce problème, l'utilisation du protocole d'affichage *wayland* peut être favorisé à *Xorg* qui est basé sur la librairie X11.

* Lors de perte de connexion avec un client, il n'y a pas de possibilité de reconnexions
