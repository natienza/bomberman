#include "BM_JStark.h"

BM_JStark::BM_JStark()
{
    nom = "JStark";
    idTexture = persoJStark;
}

BM_JStark::BM_JStark(const BM_Joueur& newJoueur)
{
    nom = "JStark";
    idTexture = persoJStark;
}

BM_JStark::BM_JStark(const BM_JStark& newJoueur)
{
    nom = "JStark";
}


BM_JStark::~BM_JStark()
{
    //dtor
}

string BM_JStark::toString(){
    return nom;
}

void BM_JStark::afficher(){
    cout << nom << endl;
}
