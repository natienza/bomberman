#include "BM_Bombe.h"

/*Constructeur/Destructeur*/
BM_Bombe::BM_Bombe(){
  duree = 3;
  portee = 2;
  motif = '+';
  texture = "texture/bombe.bmp";
  idTexture = bombe;
  poseur = new BM_JStark();
}

BM_Bombe::BM_Bombe(const BM_Bombe& newBombe){
  duree = newBombe.duree;
  portee = newBombe.portee;
  motif = newBombe.motif;
  idPoseur = newBombe.idPoseur;
  texture = "texture/bombe.bmp";
  idTexture = bombe;
}

BM_Bombe::~BM_Bombe(){
  //dtor
}

/*Comportement*/
bool BM_Bombe::isBreakable(){
  return false;
}

void BM_Bombe::setDuree(int newDuree){
  duree = newDuree;
}

void BM_Bombe::setPortee(int newPortee){
  portee = newPortee;
}

void BM_Bombe::setMotif(char newMotif){
  motif = newMotif;
}

void BM_Bombe::setId(int newId){
  idPoseur = newId;
}

void BM_Bombe::setIdCase(int newId){
  idCase = newId;
}

bool BM_Bombe::isWalkable(){
  return false;
}

bool BM_Bombe::isFlammable(){
  return false;
}

/*Getter*/
int BM_Bombe::getPortee(){
  return portee;
}

int BM_Bombe::getIdCase(){
  return idCase;
}

int BM_Bombe::getDuree(){
  return duree;
}

char BM_Bombe::getMotif(){
  return motif;
}


/*Debuggage*/
string BM_Bombe::toString(){
  return symbole;
}
void BM_Bombe::setPoseur(BM_Joueur* newPoseur){
  poseur = newPoseur;
}
void BM_Bombe::afficher(){
  cout << toString() << endl;
}
void BM_Bombe::alertPoseur(){
  poseur->updateNBombe(-1);
}
