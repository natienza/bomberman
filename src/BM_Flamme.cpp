#include "BM_Flamme.h"

BM_Flamme::BM_Flamme(){
  duree = 1;
  motif = '+';
  texture = "texture/FeuBombe.bmp";
  idTexture = feuBombe;
}

BM_Flamme::BM_Flamme(const BM_Flamme& newFlamme){
  texture = "texture/FeuBombe.bmp";
  idTexture = feuBombe;
}

BM_Flamme::~BM_Flamme(){
  
}

bool BM_Flamme::isBreakable(){
  return false;
}

string BM_Flamme::toString(){
  return symbole;
}

void BM_Flamme::afficher(){
  cout << toString() << endl;
}

int BM_Flamme::getIdCase(){
  return idCase;
}

void BM_Flamme::setDuree(float newDuree){
  duree = newDuree;
};

void BM_Flamme::setIdCase(int newId){
  idCase = newId;
}

float BM_Flamme::getDuree(){
  return duree;
}

bool BM_Flamme::isWalkable(){
  return false;
}

bool BM_Flamme::isFlammable(){
  return false;
}
