#include "BM_Server.h"

BM_Server::BM_Server(){
  fenetre.close();
  string buffer;
  cout << endl << "Lancement d'une session de jeu Bomberman..." << endl;
  cout << endl;
  cout << "Port d'écoute du serveur [défaut = 53000] : ";
  getline(cin, buffer);
  if(buffer.empty()){
    port = 53000;
  }
  else{
    port = stoi(buffer);
  }
  cout << endl;
  cout << "Nombre de joueurs à accueillir [défaut = 1] : ";
  getline(cin, buffer);
  if (buffer.empty()){
    nClients = 1;
  }
  else{
    nClients = stoi(buffer);
  }
  cout << endl;
  cout << "Souhaitez-vous afficher le jeu ? [Y/n] ";
  getline(cin, buffer);
  if (not(buffer.empty())){
    if (buffer == "n"){
      display = false;
    }
  }
  threadReception = new thread[nClients];
  threadEmission = new thread[nClients];
  client = new TcpSocket[nClients];
  idClient = new int[nClients];
  idTexture = new int[nClients];
  codeTouche = new int[nClients];
  mPaquet = new mutex[nClients];
  paquet = new Packet[nClients];
  for (int i=0; i<nClients; i++){
    client[i].setBlocking(false);
    idClient[i] = i+1;
    idTexture[i] = 0;
    codeTouche[i] = 0;
  }
  listener.setBlocking(false);
}

BM_Server::~BM_Server(){
  delete threadEmission;
  delete threadReception;
  delete client;
  delete idClient;
  delete idTexture;
  delete codeTouche;
  delete mPaquet;
  delete paquet;
}

void BM_Server::launch(){
  int i = 0;
  while(listener.listen(port)!=Socket::Done){
    cout << "Impossible d'écouter le port " << port << endl;
    this_thread::sleep_for(chrono::milliseconds(1000));
  }
  while(i < nClients){
    cout << "En attente de nouveaux clients : ... " << i << "/" << nClients << "Connectés" << endl;
    while(listener.accept(client[i]) !=Socket::Done){
      this_thread::sleep_for(chrono::milliseconds(1000));
    }
    cout << "Nouveau Client connecté depuis " << client[i].getRemoteAddress() << ":" << client[i].getRemotePort() << endl;
    selector.add(client[i]);
    i++;
  }
  listener.close();
  if (display){
    fenetre.create(VideoMode(TEXTURE_SIZE*nColonnes, TEXTURE_SIZE*nLignes), "Bomberman Alpha");
    fenetre.setFramerateLimit(60);
  }
}

void BM_Server::run(){
  for (int i=0; i<nClients; i++){
    threadReception[i] = thread(&BM_Server::receive, this, i);
    threadEmission[i] = thread(&BM_Server::send, this, i);
  }
  if (display)
    displayThread = thread(&BM_SLee::show, this);
  joueurThread = thread(&BM_Server::updateJoueur, this);
  bombThread = thread(&BM_SLee::bombWatcher, this);
  flameThread = thread(&BM_SLee::flameWatcher, this);
  cout << "running SLee..." << endl;
  bombThread.join();
  if (display)
    displayThread.join();
  for (int i=0; i<nClients; i++){
    threadEmission[i].join();
    threadReception[i].join();
  }
  flameThread.join();
  joueurThread.join();
}

void BM_Server::receive(int i){
  while(fenetre.isOpen() or gameOn){
    if (selector.wait(sf::seconds(10.f))){
      mPaquet[i].lock();
      if (client[i].receive(paquet[i]) == Socket::Done){
        paquet[i] >> codeTouche[i];
        mGrille.lock();
        if (codeTouche[i] & 1)
          tabJoueur[i]->updatePosition(0,-JOUEUR_SPEED);
        if (codeTouche[i] & 2)
          tabJoueur[i]->updatePosition(0,+JOUEUR_SPEED);
        if (codeTouche[i] & 4)
          tabJoueur[i]->updatePosition(-JOUEUR_SPEED, 0);
        if (codeTouche[i] & 8)
          tabJoueur[i]->updatePosition(+JOUEUR_SPEED, 0);
        if (codeTouche[i] & 16)
          tabJoueur[i]->setBomb();
        mGrille.unlock();
      }
      mPaquet[i].unlock();
    }
    this_thread::sleep_for(chrono::milliseconds(PERIOD_JOUEUR_WATCHER));
  }
}

void BM_Server::send(int i){
  int textureId[nLignes*nColonnes];
  while (fenetre.isOpen() or gameOn){
    mPaquet[i].lock();
    mGrille.lock();
    paquet[i].clear();
    for (int k=0; k<nLignes*nColonnes; k++){
      textureId[k] = (int) grille[k]->getTextureId();
      paquet[i] << textureId[k];

    }
    for (int k=0; k<nJoueurs; k++){
      paquet[i] << (int) tabJoueur[k]->getPosition()[0] << (int) tabJoueur[k]->getPosition()[1];
    }
    mGrille.unlock();
    if (client[i].send(paquet[i]) != Socket::Done){
      cout << "Echec de contact avec le client " << i << endl;
    }
    mPaquet[i].unlock();
    this_thread::sleep_for(chrono::milliseconds(30));
  }
}

void BM_Server::updateJoueur(){
  while(fenetre.isOpen() or gameOn){
    Event event;
    while(fenetre.pollEvent(event)){
      if (event.type==sf::Event::Closed){
        fenetre.close();
        gameOn = false;
      }
    }
    this_thread::sleep_for(chrono::milliseconds(PERIOD_JOUEUR_WATCHER));
  }
}

