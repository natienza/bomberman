#include "BM_CaseVide.h"

BM_CaseVide::BM_CaseVide()
{
    symbole = "   ";
    texture = "texture/sol.bmp";
    idTexture = sol;
}

BM_CaseVide::BM_CaseVide(string newNom)
{
    nom = newNom;
    symbole = "   ";
    texture = "texture/sol.bmp";
    idTexture = sol;
}

BM_CaseVide::~BM_CaseVide()
{
    //dtor
}

string BM_CaseVide::toString(){
    return symbole;
}

void BM_CaseVide::afficher(){
    cout << symbole << endl;
}

bool BM_CaseVide::isBreakable(){
        return false;
}
bool BM_CaseVide::isWalkable(){
  return true;
}

bool BM_CaseVide::isFlammable(){
  return true;
}
