#include "BM_Case.h"

BM_Case::BM_Case()
{
    nom = "case abstraite";
}

BM_Case::BM_Case(string newNom){
    nom = newNom;
}

void BM_Case::setTexture(string newTexture){
  texture = newTexture;
}

string BM_Case::getTexture(){
  return texture;
}

void BM_Case::setTextureId(TextureId newId){
  idTexture = newId;
}

TextureId BM_Case::getTextureId(){
  return idTexture;
}

BM_Case::~BM_Case()
{
    //dtor
}

