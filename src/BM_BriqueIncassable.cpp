#include "BM_BriqueIncassable.h"

BM_BriqueIncassable::BM_BriqueIncassable()
{
    nom = "brique incassable";
    symbole = " O ";
    texture = "texture/BriqueIncassable.bmp";
    idTexture = briqueIncassable;
}

BM_BriqueIncassable::BM_BriqueIncassable(string newNom){
    nom = newNom;
    symbole = " O ";
    texture = "texture/BriqueIncassable.bmp";
    idTexture = briqueIncassable;
}

BM_BriqueIncassable::~BM_BriqueIncassable()
{
    //dtor
}

void BM_BriqueIncassable::afficher(){
    cout << toString() << endl;
}

string BM_BriqueIncassable::toString(){
    return symbole;
}

bool BM_BriqueIncassable::isBreakable(){
    return false;
}
bool BM_BriqueIncassable::isWalkable(){
  return false;
}

bool BM_BriqueIncassable::isFlammable(){
  return false;
}
