#include "BM_Explosion.h"

BM_Explosion::BM_Explosion(){
  
}

BM_Explosion::BM_Explosion(const BM_Explosion& newExplosion){
  
}

BM_Explosion::~BM_Explosion(){
  for (int i=0; i<nFlamme; i++){
    delete tabFlamme[i];
  }
}

void BM_Explosion::setDuree(float newDuree){
  duree = newDuree;
}

float BM_Explosion::getDuree(){
  return duree;
}

int BM_Explosion::getNFlammes(){
  return nFlamme;
}

int* BM_Explosion::getIndices(){
  int * result = new int[nFlamme];
  for (int i=0; i<nFlamme; i++){
    result[i] = tabFlamme[i]->getIdCase();
  }
  return result;
}

bool BM_Explosion::containJoueur(BM_Joueur* joueur){
  for (int i=0; i<nFlamme; i++){
    if (joueur->getIdCase() == tabFlamme[i]->getIdCase()) {
      joueur->setTexture(tabFlamme[i]->getTexture());
      return true;
    }
  }
  return false;
}

void BM_Explosion::addFlamme(BM_Flamme* newFlamme){
  BM_Flamme** buffer = new BM_Flamme*[++nFlamme];
  for (int i=0; i<nFlamme-1; i++){
    buffer[i] = tabFlamme[i];
  }
  buffer[nFlamme-1] = newFlamme;
  //delete tabFlamme;
  tabFlamme = buffer;
  return;
  }
