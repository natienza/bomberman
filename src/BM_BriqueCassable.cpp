#include "BM_BriqueCassable.h"

BM_BriqueCassable::BM_BriqueCassable(){
  texture = "texture/BriqueCassable.bmp";
  idTexture = briqueCassable;
}

BM_BriqueCassable::BM_BriqueCassable(const BM_BriqueCassable& newBrique){
  texture = "texture/BriqueCassable.bmp";
  idTexture = briqueCassable;
}

BM_BriqueCassable::~BM_BriqueCassable(){

}

bool BM_BriqueCassable::isBreakable(){
  return true;
}

string BM_BriqueCassable::toString(){
  return symbole;
}

void BM_BriqueCassable::afficher(){
  cout << toString() << endl;
}

bool BM_BriqueCassable::isWalkable(){
  return false;
}

bool BM_BriqueCassable::isFlammable(){
  return false;
}
