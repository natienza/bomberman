#include "BM_Client.h"


BM_Client::BM_Client(){
  string buffer;
  fenetre.close();
  socket.setBlocking(false);
  cout << "Adresse du serveur de jeu [défaut = 127.0.0.1] : ";
  getline(cin, adressServer);
  if (adressServer.empty()){
    adressServer = "127.0.0.1";
  }
  cout << endl;
  cout << "Port du serveur de jeu [défaut = 53000] : ";
  getline(cin, buffer);
  if (buffer.empty()){
    port = 53000;
  }
  else{
    port = stoi(buffer);
  }
  cout << endl;
}

BM_Client::~BM_Client(){
}

void BM_Client::connect(){
  cout << "Tentative de connexion au serveur " << adressServer << ":" << port << endl;
  while (socket.connect(adressServer, port) != sf::Socket::Done){
    cout << "En attente de réponse du serveur" << endl;
    this_thread::sleep_for(chrono::milliseconds(1000));
  }
  cout << "Client connecté à " << socket.getRemoteAddress() << ":" << socket.getRemotePort()<< endl;
  
}
void BM_Client::run(){
  paquet.clear();
  cout << "En attente du lancement du jeux..." << endl;
  while(socket.receive(paquet)!=Socket::Done){
    
  }
  fenetre.create(VideoMode(TEXTURE_SIZE*nColonnes, TEXTURE_SIZE*nLignes), "Bomberman Client Alpha");
  fenetre.setFramerateLimit(60);
  cout << "Jeux lancé !" << endl;
  joueurThread = thread(&BM_Client::joueurWatcher, this);
  updateThread = thread(&BM_Client::updateServeur, this);
  threadReception = thread(&BM_Client::receive, this);
  displayThread = thread(&BM_SLee::show, this);
  threadReception.join();
  displayThread.join();
  joueurThread.join();
  updateThread.join();
}


void BM_Client::joueurWatcher(){
  Texture texture;
  bool verrouJ1 = false;
  while(fenetre.isOpen()){
    sf::Event event;
    while (fenetre.pollEvent(event))
      {
        if (event.type == sf::Event::Closed)
          fenetre.close();
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
          codeTouche |= 1;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
          codeTouche |= 2;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
          codeTouche |= 4;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
          codeTouche |= 8;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) and not(verrouJ1)){
          codeTouche = 16;
          verrouJ1 = true;
        }
      }
    verrouJ1 = false;
    this_thread::sleep_for(chrono::milliseconds(PERIOD_JOUEUR_WATCHER));
  }
}

void BM_Client::updateServeur(){
  while(fenetre.isOpen()){
    mPaquet.lock();
    paquet.clear();
    paquet << codeTouche;
    if (socket.send(paquet) == sf::Socket::Done){
      codeTouche = 0;
    }
    else{
      cout << "Echec de contact avec le serveur" << endl;
    }
    mPaquet.unlock();
    this_thread::sleep_for(chrono::milliseconds(30));
  }
}

void BM_Client::receive(){
  int textureId[nLignes*nColonnes];
  while(fenetre.isOpen()){
    mGrille.lock();
    mPaquet.lock();
    if (socket.receive(paquet) == Socket::Done){
      mPaquet.unlock();
      for (int k=0; k<nLignes*nColonnes; k++){
        paquet>>textureId[k];
        grille[k]->setTextureId(TextureId(textureId[k]));
      }
      int position1;
      int position2; 
      for (int k=0; k<nJoueurs; k++){
        paquet >> position1 >> position2;
        tabJoueur[k]->setPosition(position1, position2);
      }
    }
    else{
      cout << "Pas de reception du serveur" << endl;
    }
    mGrille.unlock();
    mPaquet.unlock();
    this_thread::sleep_for(chrono::milliseconds(30));
  }
}
