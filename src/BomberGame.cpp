#include "BomberGame.h"


BomberGame::BomberGame(){
  fenetre.create(sf::VideoMode(800, 600), "Bomberman Beta");
  fenetre.setFramerateLimit(60);
  partie = new BM_SLee(nLignes,nColonnes);
}



BomberGame::~BomberGame(){
  delete partie;
}
void BomberGame::show(){
  sf::Texture texture;
  sf::RectangleShape Case(sf::Vector2f(60.f, 60.f));
  for (int i = 0; i<nLignes; i++){
    for (int j=0; j<nColonnes; j++){
      texture.loadFromFile("texture/bombe.bmp");
      Case.setTexture(&texture);
      Case.setPosition(i+60, j+60);
      fenetre.draw(Case);
    }
  }
  while (fenetre.isOpen())
    {
      for (int i = 0; i<nLignes; i++){
        for (int j=0; j<nColonnes; j++){
          texture.loadFromFile(partie->getCase(i,j)->getTexture());
          Case.setTexture(&texture);
          Case.setPosition(j*60, i*60);
          fenetre.draw(Case);
        }
  }
      sf::Event event;
      while (fenetre.pollEvent(event))
        {
          if (event.type == sf::Event::Closed)
            fenetre.close();
        }
  fenetre.display();
}
}
