#include "BM_Joueur.h"
#include "BM_SLee.h"

BM_Joueur::BM_Joueur()
{
    nom = "test";
}

BM_Joueur::BM_Joueur(const BM_Joueur& newJoueur)
{
    nom = "test";
    //nom = newJoueur.nom;
}

BM_Joueur::~BM_Joueur()
{
    //dtor
}

void BM_Joueur::setTextureId(TextureId newId){
  idTexture = newId;
}

TextureId BM_Joueur::getTextureId(){
  return idTexture;
}

void BM_Joueur::setPosition(float newLigne, float newColonne){
    position[0] = newLigne;
    position[1] = newColonne;
    idCase = position[1] + grille->getNColonnes()*position[0];
}

float* BM_Joueur::getPosition(){
  return position;
}

void BM_Joueur::updatePosition(float deltaLigne, float deltaColonne){
  if (grille->getCase((int) (position[0]+deltaLigne/JOUEUR_SPEED) ,(int) (position[1] + deltaColonne/JOUEUR_SPEED))->isWalkable()){
    position[0] += deltaLigne;
    position[1] += deltaColonne;
    idCase = position[1] + grille->getNColonnes()*position[0];
  }
}

void BM_Joueur::setBomb(){
  if (nBomb < NB_MAX_BOMB){
    this->nBomb++;
    BM_Bombe* bomb = new BM_Bombe();
    bomb->setPoseur(this);
    grille->setCase(position[0], position[1], bomb);
    bomb->setIdCase(idCase);
    grille->addBombe(bomb);
    #ifdef VERBOSE
    cout << "Création d'un bombe" << endl;
    #endif
  }
}

void BM_Joueur::updateNBombe(int up){
  nBomb += up;
}

void BM_Joueur::setTexture(string newTexture){
  texture = newTexture;
}

string BM_Joueur::getTexture(){
  return texture;
}

int BM_Joueur::getIdCase(){
  return idCase;
}

void BM_Joueur::setCase(BM_Case* newCase){
  actualCase = newCase;
}

BM_Case* BM_Joueur::getCase(){
  return actualCase;
}

void BM_Joueur::setGrille(BM_SLee* newGrille){
  grille = newGrille;
}
