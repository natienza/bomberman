#include <cstdlib>
#include <ctime>

#include "BM_SLee.h"
#include "BM_CaseVide.h"
#include "BM_BriqueIncassable.h"
#include "BM_BriqueCassable.h"
#include "BM_Bombe.h"

/*Constructeur*/

BM_SLee::BM_SLee():BM_SLee(16,20){
 
 }

BM_SLee::BM_SLee(int newNLignes, int newNColonnes)
{
  #ifdef VERBOSE
  cout << "Creation d'un SLee de dimension " 
    << newNLignes << "*" << newNColonnes 
    << endl;
	#endif // VERBOSE
	
  // Les dimensions de la grille doivent être impaire pour la remplir
  if (not(newNColonnes&1)) newNColonnes++;
  if (not(newNLignes&1)) newNLignes++;
  nColonnes = newNColonnes;
  nLignes = newNLignes;
    
  // Joueurs
  nJoueurs = 0;
  #ifdef VERBOSE
  cout << "-- Chargement des textures en RAM" << endl;
  #endif //VERBOSE
  //// Creation des texture
  for (int i=0; i<NB_TEXTURE; i++){
    tabTexture[i] = new Texture;
  }
  //// Lecture depuis les fichiers
  tabTexture[sol]->loadFromFile("./texture/sol.bmp");
  tabTexture[briqueIncassable]->loadFromFile("./texture/BriqueIncassable.bmp");
  tabTexture[briqueCassable]->loadFromFile("./texture/BriqueCassable.bmp");
  tabTexture[bombe]->loadFromFile("./texture/bombe.bmp");
  tabTexture[feuBombe]->loadFromFile("./texture/FeuBombe.bmp");
  tabTexture[feuH]->loadFromFile("./texture/FeuH.bmp");
  tabTexture[feuV]->loadFromFile("./texture/FeuV.bmp");
  tabTexture[feuEx]->loadFromFile("./texture/FeuEx.bmp");
  tabTexture[feuBrique]->loadFromFile("./texture/FeuBrique.bmp");
  tabTexture[bonusBombe]->loadFromFile("./texture/BonusBombe.bmp");
  tabTexture[bonusCoupPied]->loadFromFile("./texture/BonusCoupPied.bmp");
  tabTexture[bonusFlamme]->loadFromFile("./texture/BonusFlamme.bmp");
  tabTexture[bonusRoller]->loadFromFile("./texture/BonusRoller.bmp");
  tabTexture[persoJStark]->loadFromFile("./texture/perso3.png");
 
  // Grille
  #ifdef VERBOSE
    cout << "-- Mise en place de la grille" << endl;
	#endif // VERBOSE
  int i,j;
  grille = new BM_Case*[nLignes*nColonnes];
  //// murs horizontaux
  #ifdef VERBOSE
  cout << "---- initialisation des murs horizontaux" << endl;
	#endif // VERBOSE
  for (j = 0; j < nColonnes; j++) {
    grille[j] = new BM_BriqueIncassable();
    grille[j+nColonnes*(nLignes-1)] = new BM_BriqueIncassable();
	}
	//// murs verticaux
  #ifdef VERBOSE
    cout << "---- initialisation des murs verticaux" << endl;
	#endif // VERBOSE
	for (i = 1; i < nLignes-1; i++) {
		grille[i*nColonnes] = new BM_BriqueIncassable();
		grille[(i+1)*nColonnes-1] = new BM_BriqueIncassable();
	}
    //// fond vide
  #ifdef VERBOSE
    cout << "---- initialisation du fond vide" << endl;
	#endif // VERBOSE
	for (i = 1; i < nLignes-1; i++) {
		for (j = 1; j < nColonnes-1; j++) {
			grille[i*nColonnes+j] = new BM_CaseVide();
		}
	}    
	//// murs centraux
  #ifdef VERBOSE
    cout << "---- initialisation des murs centraux" << endl;
	#endif // VERBOSE
	for (i = 2; i < nLignes-2; i+=2) {
		for (j = 2; j < nColonnes-2; j+=2) {
			delete grille[i*nColonnes+j];
			grille[i*nColonnes+j] = new BM_BriqueIncassable();
		}
	}
	//// decors
  #ifdef VERBOSE
    cout << "---- initialisation des decors" << endl;
	#endif // VERBOSE
	/*delete grille[nColonnes+1];
	grille[nColonnes+1] = new BM_BriqueCassable();
	delete grille[nColonnes+4];
	grille[nColonnes+4] = new BM_BriqueCassable();
	delete grille[nColonnes+5];
	grille[nColonnes+5] = new BM_BriqueCassable();
	delete grille[2*nColonnes+3];
	grille[2*nColonnes+3] = new BM_BriqueCassable();*/
  genZoneJeu();
	//// bombe
  #ifdef VERBOSE
    cout << "---- initialisation des bombes" << endl;
	#endif // VERBOSE
	/*BM_Bombe* bombe = new BM_Bombe();
	delete grille[nColonnes+3];
	grille[nColonnes+3] = bombe;
	bombe->setIdCase(nColonnes+3);
	addBombe(bombe);*/
    
    // Fenetre
    #ifdef VERBOSE
    cout << "-- Création de la fenetre" << endl;
	#endif // VERBOSE
    if (display){
      fenetre.create(VideoMode(TEXTURE_SIZE*nColonnes, TEXTURE_SIZE*nLignes), "Bomberman Alpha");
      fenetre.setFramerateLimit(60);
    }
    #ifdef VERBOSE
    cout << "-- Création de la table de Joueur initiale" << endl;
    #endif //VERBOSE

    tabJoueur = new BM_Joueur*[2];
    tabJoueur[0] = new BM_JStark();
    tabJoueur[1] = new BM_JStark();
    nJoueurs = 2;
    tabJoueur[0]->setGrille(this);
    tabJoueur[1]->setGrille(this);
    tabJoueur[0]->setPosition(1,1);
    tabJoueur[1]->setPosition(1,nColonnes-2);
    #ifdef VERBOSE
    cout << "-- Fin de la création de SLee" << endl;
	#endif // VERBOSE
}

/*Setter*/

void BM_SLee::setDimX(int newNColonnes){
    nColonnes = newNColonnes;
}

void BM_SLee::setDimY(int newNLignes){
    nLignes = newNLignes;
}


void BM_SLee::addExplosion(BM_Explosion* newExplosion){
  BM_Explosion** buffer = new BM_Explosion*[++nExplosion];
  for (int i=0; i<nExplosion-1; i++){
    buffer[i] = tabExplosion[i];
  }
  buffer[nExplosion-1] = newExplosion;
  delete [] tabExplosion;
  tabExplosion = buffer;
  return; 
}

void BM_SLee::addBombe(BM_Bombe* newBombe){
  if (nBombe > 0){
    BM_Bombe** buffer = new BM_Bombe*[++nBombe];
    for (int i=0; i<nBombe-1; i++){
      buffer[i] = tabBombe[i];
    }
    buffer[nBombe-1] = newBombe;
    delete [] tabBombe;
    tabBombe = new BM_Bombe*[nBombe];
    tabBombe = buffer;
    return;
  }
  tabBombe = new BM_Bombe*[1];
  tabBombe[0] = newBombe;
  nBombe++;
  return;
}

void BM_SLee::delFlamme(BM_Flamme* flamme, int position){
  int indiceCase = flamme->getIdCase();
  for (int i=position+1; i<nFlamme; i++){
    tabFlamme[i-1] = tabFlamme[i];
  }
  nFlamme--;
  char* nomCase = new char[2];
  sprintf(nomCase, "%3d", indiceCase);
  delete grille[indiceCase];
  grille[indiceCase] = new BM_CaseVide((string) nomCase);
  delete [] nomCase;
}

void BM_SLee::delExplosion(BM_Explosion* explosion, int position){
  for (int i=position+1; i<nExplosion; i++){
    tabExplosion[i-1] = tabExplosion[i];
  }
  nExplosion--;
  int * index = explosion->getIndices();
  for (int i=0; i<explosion->getNFlammes(); i++){
    delete grille[index[i]];
    grille[index[i]] = new BM_CaseVide();
  }
}

void BM_SLee::delJoueur(int position){
  delete tabJoueur[position];
  for (int i=position+1; i<nJoueurs; i++){
    tabJoueur[i-1] = tabJoueur[i];
  }
  nJoueurs--;
}

void BM_SLee::delBombe(int position){
  BM_Bombe* bombe = tabBombe[position];
  bombe->alertPoseur();
  char motif = bombe->getMotif();
  int P = bombe->getPortee();
  int indiceCase = bombe->getIdCase();
  for (int i=position+1; i<nBombe; i++){
    tabBombe[i-1] = tabBombe[i];
  }
  nBombe--;
  BM_Explosion * currentExplosion = new BM_Explosion();
  addExplosion(currentExplosion);
  char* nomCase = new char[2];
  sprintf(nomCase, "%3d", indiceCase);
  BM_Flamme* flamme = new BM_Flamme();
  delete grille[indiceCase];
  grille[indiceCase] = flamme;
  flamme->setIdCase(indiceCase);
  currentExplosion->addFlamme(flamme);
  int i = indiceCase;
  switch(motif){
  case '+':
    int p = 1;
    bool doneXD = false;
    bool doneXG = false;
    bool doneYH = false;
    bool doneYB = false;
    bool doneX = false;
    bool doneY = false;
    while(not(doneX and doneY)){
      if (not(doneXD)){
        if (i+p>0 and i+p<nColonnes*nLignes){
          if (grille[i+p]->isBreakable()){
            delete grille[i+p];
            grille[i+p] = new BM_Flamme();
            static_cast<BM_Flamme*>(grille[i+p])->setIdCase(i+p);
            currentExplosion->addFlamme(static_cast<BM_Flamme*>(grille[i+p]));
            grille[i+p]->setTextureId(feuBrique);
            doneXD = true;
          }
          else if (grille[i+p]->isFlammable()){
            delete grille[i+p];
            grille[i+p] = new BM_Flamme();
            static_cast<BM_Flamme*>(grille[i+p])->setIdCase(i+p);
            currentExplosion->addFlamme(static_cast<BM_Flamme*>(grille[i+p]));
            if (p>=P){
              grille[i+p]->setTextureId(feuH);
              doneXD = true;
            }
            else{
              grille[i+p]->setTextureId(feuH);
            }
          }
          else {
            doneXD = true;
          }
        }
      }
      if (not(doneXG)){
        if (i-p>0 and i-p<nColonnes*nLignes){
          if (grille[i-p]->isBreakable()){
            delete grille[i-p];
            grille[i-p] = new BM_Flamme();
            static_cast<BM_Flamme*>(grille[i-p])->setIdCase(i-p);
            currentExplosion->addFlamme(static_cast<BM_Flamme*>(grille[i-p]));
            grille[i-p]->setTextureId(feuBrique);
            doneXG = true;
          }
          else if (grille[i-p]->isFlammable()){
            delete grille[i-p];
            grille[i-p] = new BM_Flamme();
            static_cast<BM_Flamme*>(grille[i-p])->setIdCase(i-p);
            currentExplosion->addFlamme(static_cast<BM_Flamme*>(grille[i-p]));
            if (p>=P){
              grille[i-p]->setTextureId(feuEx);
              doneXG = true;
            }
            else{
              grille[i-p]->setTextureId(feuH);
            }
          }
          else {
            doneXG = true;
          }
        }
      }
      if (not(doneYH)){
        if (i+p*nColonnes>0 and i+p*nColonnes<nColonnes*nLignes){
          if (grille[i+p*nColonnes]->isBreakable()){
            delete grille[i+p*nColonnes];
            grille[i+p*nColonnes] = new BM_Flamme();
            static_cast<BM_Flamme*>(grille[i+p*nColonnes])->setIdCase(i+p*nColonnes);
            currentExplosion->addFlamme(static_cast<BM_Flamme*>(grille[i+p*nColonnes]));
            grille[i+p*nColonnes]->setTextureId(feuBrique);
            doneYH = true;
          }
          else if (grille[i+p*nColonnes]->isFlammable()){
            delete grille[i+p*nColonnes];
            grille[i+p*nColonnes] = new BM_Flamme();
            static_cast<BM_Flamme*>(grille[i+p*nColonnes])->setIdCase(i+p*nColonnes);
            currentExplosion->addFlamme(static_cast<BM_Flamme*>(grille[i+p*nColonnes]));
            if (p>=P){
              grille[i+p*nColonnes]->setTextureId(feuV);
              doneYH = true;
            }
            else{
              grille[i+p*nColonnes]->setTextureId(feuV);
            }
          }
          else {
            doneYH = true;
          }
        }
      }
      if (not(doneYB)){
        if (i-p*nColonnes>0 and i-p*nColonnes<nColonnes*nLignes){
          if (grille[i-p*nColonnes]->isBreakable()){
            delete grille[i-p*nColonnes];
            grille[i-p*nColonnes] = new BM_Flamme();
            static_cast<BM_Flamme*>(grille[i-p*nColonnes])->setIdCase(i-p*nColonnes);
            currentExplosion->addFlamme(static_cast<BM_Flamme*>(grille[i-p*nColonnes]));
            grille[i-p*nColonnes]->setTextureId(feuBrique);
            doneYB = true;
          }
          else if (grille[i-p*nColonnes]->isFlammable()){
            delete grille[i-p*nColonnes];
            grille[i-p*nColonnes] = new BM_Flamme();
            static_cast<BM_Flamme*>(grille[i-p*nColonnes])->setIdCase(i-p*nColonnes);
            currentExplosion->addFlamme(static_cast<BM_Flamme*>(grille[i-p*nColonnes]));
            if (p>=P){
              grille[i-p*nColonnes]->setTextureId(feuV);
              doneYB = true;
            }
            else{
              grille[i-p*nColonnes]->setTextureId(feuV);
            }
          }
          else {
            doneYB = true;
          }
        }
      }
      doneX = doneXG and doneXD;
      doneY = doneYH and doneYB;
      p++;
    }
    break;
  }
}
/*Getter*/

BM_Case* BM_SLee::getCase(int ligne, int colonne){
  return grille[colonne + ligne*nColonnes];
}

void BM_SLee::setCase(int ligne, int colonne, BM_Case* newCase){
  delete grille[colonne+ligne*nColonnes];
  grille[colonne+ligne*nColonnes] = newCase;
}

int BM_SLee::getNColonnes(){
  return nColonnes;
}

/* Génération des ZoneJeu */
void BM_SLee::genZoneJeu()
{
  int tailleZJ = 3*(nColonnes-3)*(nLignes-3)/4+nColonnes+nLignes-17;
  int tabIdCasesZJ[tailleZJ];
  int i, j, k;
  k = 0;

  // Liste des indices des cases de la zone de jeu
  for (j=3; j<nColonnes-3; j++)
  {
    tabIdCasesZJ[k++] = nColonnes+j;
    tabIdCasesZJ[k++] = (nLignes-2)*nColonnes+j;
  }
  for (j=3; j<nColonnes-3; j+=2)
  {
    tabIdCasesZJ[k++] = 2*nColonnes+j;
    tabIdCasesZJ[k++] = (nLignes-3)*nColonnes+j;
  }
  for (i=3; i<nLignes-4; i+=2) 
  {
    for (j=1; j<nColonnes-1; j++) // Lignes impaires
    {
      tabIdCasesZJ[k++] = i*nColonnes+j;
    }
    for (j=1; j<nColonnes-1; j+=2) // Lignes paires
    {
      tabIdCasesZJ[k++] = (i+1)*nColonnes+j;
    }
  }
  for (j=1; j<nColonnes-1; j++) // Lignes impaires
  {
    tabIdCasesZJ[k++] = (nLignes-4)*nColonnes+j;
  }

  // Melange de la liste
  int r;
  int temp;
  for(k = 0; k<tailleZJ;k++){
    r=rand()%(tailleZJ);
    // On échange les contenus des cases k et r
    temp = tabIdCasesZJ[k];
    tabIdCasesZJ[k] = tabIdCasesZJ[r];
    tabIdCasesZJ[r]=temp;
  }

  // Pose des briques cassables
  for (k=0; k<DECOR_NB_CASSABLE; k++){
    delete grille[tabIdCasesZJ[k]];
    grille[tabIdCasesZJ[k]] = new BM_BriqueCassable();
  }
}

/*Destructeur*/

BM_SLee::~BM_SLee()
{
  #ifdef VERBOSE
  cout << "Destructeur" << endl;
  cout << "Destruction joueur" << endl;
  #endif
  for (int i=0; i<nJoueurs; i++){
    delete tabJoueur[i];
  }
  #ifdef VERBOSE
  cout << "destruction grille" << endl;
  #endif
  for (int i=0; i<nLignes*nColonnes; i++){
    delete grille[i];
  }
  #ifdef VERBOSE
  cout << "destruction Bombe" << endl;
  #endif
  for (int i=0; i<nBombe; i++){
    cout << "bombe " << i << endl;
    delete tabBombe[i];
  }
  #ifdef VERBOSE
  cout << "destruction explosion" << endl;
  #endif
  for (int i=0; i<nExplosion; i++){
    delete tabExplosion[i];
  }
  #ifdef VERBOSE
  cout << "Fin du destructeur" << endl;
  #endif
}

/*Debuggage*/

void BM_SLee::show(){
  Texture texture;
  RectangleShape Case(Vector2f(TEXTURE_SIZE, TEXTURE_SIZE));
  while (fenetre.isOpen())
    {
      mGrille.lock();
      for (int i = 0; i<nLignes; i++){
        for (int j=0; j<nColonnes; j++){
          Case.setTexture(tabTexture[getCase(i,j)->getTextureId()]);
          Case.setPosition(j*TEXTURE_SIZE, i*TEXTURE_SIZE);
          fenetre.draw(Case);
        }
      }
      RectangleShape Case(Vector2f(TEXTURE_SIZE, 55.f));
      for (int n=0; n<nJoueurs; n++){
        Case.setTexture(tabTexture[tabJoueur[n]->getTextureId()]);
        Case.setPosition(tabJoueur[n]->getPosition()[1]*TEXTURE_SIZE, tabJoueur[n]->getPosition()[0]*TEXTURE_SIZE-21);
        fenetre.draw(Case);
      }
      fenetre.display();
      mGrille.unlock();
      this_thread::sleep_for(std::chrono::milliseconds(40));
        }
}

void BM_SLee::joueurWatcher(){
  Texture texture;
  bool verrouJ1 = false;
  bool verrouJ2 = false;
  RectangleShape Case(Vector2f(TEXTURE_SIZE, TEXTURE_SIZE));
  while(fenetre.isOpen()){
    sf::Event event;
    while (fenetre.pollEvent(event))
      {
        if (event.type == sf::Event::Closed){
          fenetre.close();
          gameOn = false;
        }
        if (nJoueurs>0){
          if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
            tabJoueur[0]->updatePosition(0,-JOUEUR_SPEED);
          if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
            tabJoueur[0]->updatePosition(0,+JOUEUR_SPEED);
          if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
            tabJoueur[0]->updatePosition(-JOUEUR_SPEED,0);
          if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
            tabJoueur[0]->updatePosition(JOUEUR_SPEED,0);
          if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) and not(verrouJ1)){
            tabJoueur[0]->setBomb();
            verrouJ1 = true;
          }
          if (nJoueurs>1 and not(verrouJ2)){
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
              tabJoueur[1]->updatePosition(0,-JOUEUR_SPEED);
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
              tabJoueur[1]->updatePosition(0,+JOUEUR_SPEED);
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
              tabJoueur[1]->updatePosition(-JOUEUR_SPEED,0);
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
              tabJoueur[1]->updatePosition(JOUEUR_SPEED,0);
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) and not(verrouJ2)){
              tabJoueur[1]->setBomb();
              verrouJ2 = true;
            }
            verrouJ2 = true;
          }
        }
      }
    verrouJ1 = false;
    verrouJ2 = false;
    this_thread::sleep_for(chrono::milliseconds(PERIOD_JOUEUR_WATCHER));
  }
}

void BM_SLee::bombWatcher(){
  int i=0;
  while(fenetre.isOpen() or gameOn){
    i+=PERIOD_BOMB_WATCHER;
    for (int n=0; n<nBombe; n++){
      tabBombe[n]->setDuree(tabBombe[n]->getDuree() - (float)PERIOD_BOMB_WATCHER/1000);
      if (tabBombe[n]->getDuree()<=0){
        #ifdef VERBOSE
        cout << "Bombe explosée au temps " << (float)i/1000 << "s\n" << endl;
        #endif
        cout << tabBombe[n]->getPortee() << endl;
        delBombe(n);
      }
    }
    this_thread::sleep_for(chrono::milliseconds(PERIOD_BOMB_WATCHER));
  }
}

void BM_SLee::flameWatcher(){
  while(fenetre.isOpen() or gameOn){
    this_thread::sleep_for(std::chrono::milliseconds(periodFlammeWatcher));
    for (int i=0; i<nExplosion; i++){
      tabExplosion[i]->setDuree(tabExplosion[i]->getDuree() - (float)periodFlammeWatcher/1000);
      if (tabExplosion[i]->getDuree()<=0){
        delExplosion(tabExplosion[i], i);
      }
      for (int n=0; n<nJoueurs; n++){
        if (tabExplosion[i]->containJoueur(tabJoueur[n])){
          delJoueur(n);
        }
      }
    }
  }
}

string BM_SLee::grilleToString(){
    string motif = "+";
    string result = "" ;
    for (int i=0; i<nColonnes; i++){
        motif.append("-----+");
    }
    for (int i=0; i<nLignes; i++){
        result.append(motif);
        result.append("\n| ");
        for(int j=0; j<nColonnes; j++){
            result.append(getCase(i,j)->toString());
            result.append(" | ");
        }
        result.append("\n");
    }
    result.append(motif);
    return result;
}

void BM_SLee::afficherGrille(){
    cout << this->grilleToString() << endl;
}

string BM_SLee::joueurToString(){
    string answer = "Les joueurs de la partie sont :\t";
    for (int i=0; i<nJoueurs; i++){
        answer.append(tabJoueur[i]->toString());
        answer.append("\t");
    }
    return answer;
}

void BM_SLee::afficherJoueur(){
    cout << this->joueurToString() << endl;
}
