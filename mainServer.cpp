#include <iostream>
#include <SFML/Graphics.hpp>
#include "BM_Server.h"

using namespace std;

int main()
{
  cout << "running server" << endl;
  BM_Server* server = new BM_Server();
  server->launch();
  server->run();
  delete server;
  return 0;
}
