WORKDIR = `pwd`

CC = gcc
CXX = g++
AR = ar
LD = g++
WINDRES = windres

INC = 
CFLAGS = -Wall -fexceptions
RESINC = 
LIBDIR = 
LIB = 
LDFLAGS = 

INC_DEBUG = $(INC) -Iinclude
CFLAGS_DEBUG = $(CFLAGS) -g
RESINC_DEBUG = $(RESINC)
RCFLAGS_DEBUG = $(RCFLAGS)
LIBDIR_DEBUG = $(LIBDIR)
LIB_DEBUG = $(LIB)-lsfml-graphics -lsfml-window -lsfml-system -lsfml-network -lpthread -lX11
LDFLAGS_DEBUG = $(LDFLAGS)
OBJDIR_DEBUG = obj/Debug
DEP_DEBUG = 
OUT_DEBUG = bin/Debug/Bomberman
OUT_DEBUG_SERVER = bin/Debug/Server
OUT_DEBUG_CLIENT = bin/Debug/Client

OBJ_DEBUG = $(OBJDIR_DEBUG)/main.o $(OBJDIR_DEBUG)/src/BM_BriqueIncassable.o $(OBJDIR_DEBUG)/src/BM_Case.o $(OBJDIR_DEBUG)/src/BM_CaseVide.o $(OBJDIR_DEBUG)/src/BM_JStark.o $(OBJDIR_DEBUG)/src/BM_Joueur.o $(OBJDIR_DEBUG)/src/BM_SLee.o $(OBJDIR_DEBUG)/src/BM_Bombe.o $(OBJDIR_DEBUG)/src/BM_BriqueCassable.o $(OBJDIR_DEBUG)/src/BomberGame.o $(OBJDIR_DEBUG)/src/BM_Flamme.o $(OBJDIR_DEBUG)/src/BM_Explosion.o

OBJ_DEBUG_SERVER = $(OBJDIR_DEBUG)/mainServer.o $(OBJDIR_DEBUG)/src/BM_BriqueIncassable.o $(OBJDIR_DEBUG)/src/BM_Case.o $(OBJDIR_DEBUG)/src/BM_CaseVide.o $(OBJDIR_DEBUG)/src/BM_JStark.o $(OBJDIR_DEBUG)/src/BM_Joueur.o $(OBJDIR_DEBUG)/src/BM_SLee.o $(OBJDIR_DEBUG)/src/BM_Bombe.o $(OBJDIR_DEBUG)/src/BM_BriqueCassable.o $(OBJDIR_DEBUG)/src/BomberGame.o $(OBJDIR_DEBUG)/src/BM_Flamme.o $(OBJDIR_DEBUG)/src/BM_Explosion.o $(OBJDIR_DEBUG)/src/BM_Server.o

OBJ_DEBUG_CLIENT = $(OBJDIR_DEBUG)/mainClient.o $(OBJDIR_DEBUG)/src/BM_BriqueIncassable.o $(OBJDIR_DEBUG)/src/BM_Case.o $(OBJDIR_DEBUG)/src/BM_CaseVide.o $(OBJDIR_DEBUG)/src/BM_JStark.o $(OBJDIR_DEBUG)/src/BM_Joueur.o $(OBJDIR_DEBUG)/src/BM_SLee.o $(OBJDIR_DEBUG)/src/BM_Bombe.o $(OBJDIR_DEBUG)/src/BM_BriqueCassable.o $(OBJDIR_DEBUG)/src/BomberGame.o $(OBJDIR_DEBUG)/src/BM_Flamme.o $(OBJDIR_DEBUG)/src/BM_Explosion.o $(OBJDIR_DEBUG)/src/BM_Client.o



all: debug

clean: clean_debug

before_debug: 
	test -d bin/Debug || mkdir -p bin/Debug
	test -d $(OBJDIR_DEBUG) || mkdir -p $(OBJDIR_DEBUG)
	test -d $(OBJDIR_DEBUG)/src || mkdir -p $(OBJDIR_DEBUG)/src

after_debug: 

debug: before_debug out_debug after_debug

out_debug: before_debug $(OBJ_DEBUG) $(OBJ_DEBUG_SERVER) $(OBJ_DEBUG_CLIENT) $(DEP_DEBUG)
	$(LD) $(LIBDIR_DEBUG) -o $(OUT_DEBUG) $(OBJ_DEBUG)  $(LDFLAGS_DEBUG) $(LIB_DEBUG)
	$(LD) $(LIBDIR_DEBUG) -o $(OUT_DEBUG_SERVER) $(OBJ_DEBUG_SERVER)  $(LDFLAGS_DEBUG) $(LIB_DEBUG)
	$(LD) $(LIBDIR_DEBUG) -o $(OUT_DEBUG_CLIENT) $(OBJ_DEBUG_CLIENT)  $(LDFLAGS_DEBUG) $(LIB_DEBUG)

$(OBJDIR_DEBUG)/mainServer.o: mainServer.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c mainServer.cpp -o $(OBJDIR_DEBUG)/mainServer.o

$(OBJDIR_DEBUG)/mainClient.o: mainClient.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c mainClient.cpp -o $(OBJDIR_DEBUG)/mainClient.o

$(OBJDIR_DEBUG)/main.o: main.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c main.cpp -o $(OBJDIR_DEBUG)/main.o

$(OBJDIR_DEBUG)/src/BM_BriqueIncassable.o: src/BM_BriqueIncassable.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c src/BM_BriqueIncassable.cpp -o $(OBJDIR_DEBUG)/src/BM_BriqueIncassable.o

$(OBJDIR_DEBUG)/src/BM_Case.o: src/BM_Case.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c src/BM_Case.cpp -o $(OBJDIR_DEBUG)/src/BM_Case.o

$(OBJDIR_DEBUG)/src/BM_CaseVide.o: src/BM_CaseVide.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c src/BM_CaseVide.cpp -o $(OBJDIR_DEBUG)/src/BM_CaseVide.o

$(OBJDIR_DEBUG)/src/BM_JStark.o: src/BM_JStark.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c src/BM_JStark.cpp -o $(OBJDIR_DEBUG)/src/BM_JStark.o

$(OBJDIR_DEBUG)/src/BM_Joueur.o: src/BM_Joueur.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c src/BM_Joueur.cpp -o $(OBJDIR_DEBUG)/src/BM_Joueur.o

$(OBJDIR_DEBUG)/src/BM_SLee.o: src/BM_SLee.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c src/BM_SLee.cpp -o $(OBJDIR_DEBUG)/src/BM_SLee.o

$(OBJDIR_DEBUG)/src/BM_Bombe.o: src/BM_Bombe.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c src/BM_Bombe.cpp -o $(OBJDIR_DEBUG)/src/BM_Bombe.o

$(OBJDIR_DEBUG)/src/BM_BriqueCassable.o: src/BM_BriqueCassable.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c src/BM_BriqueCassable.cpp -o $(OBJDIR_DEBUG)/src/BM_BriqueCassable.o

$(OBJDIR_DEBUG)/src/BomberGame.o: src/BomberGame.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c src/BomberGame.cpp -o $(OBJDIR_DEBUG)/src/BomberGame.o

$(OBJDIR_DEBUG)/src/BM_Flamme.o: src/BM_Flamme.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c src/BM_Flamme.cpp -o $(OBJDIR_DEBUG)/src/BM_Flamme.o

$(OBJDIR_DEBUG)/src/BM_Explosion.o: src/BM_Explosion.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c src/BM_Explosion.cpp -o $(OBJDIR_DEBUG)/src/BM_Explosion.o

$(OBJDIR_DEBUG)/src/BM_Server.o: src/BM_Server.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c src/BM_Server.cpp -o $(OBJDIR_DEBUG)/src/BM_Server.o

$(OBJDIR_DEBUG)/src/BM_Client.o: src/BM_Client.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c src/BM_Client.cpp -o $(OBJDIR_DEBUG)/src/BM_Client.o

clean_debug: 
	rm -f $(OBJ_DEBUG) $(OBJ_DEBUG_SERVER) $(OBJ_DEBUG_CLIENT) $(OUT_DEBUG)
	rm -rf bin/Debug
	rm -rf $(OBJDIR_DEBUG)
	rm -rf $(OBJDIR_DEBUG)/src

.PHONY: before_debug after_debug clean_debug

