#include <iostream>
#include <SFML/Network.hpp>

#include "BM_Client.h"
using namespace std;

int main()
{
  cout << "running client" << endl;
  BM_Client* client =new  BM_Client();
  client->connect();
  client->run();
  delete client;
  return 0;
}
